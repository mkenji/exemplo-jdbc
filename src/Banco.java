
public class Banco {
	private int codigo;
	private String cnpj;
	private String nome;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public String toString() {
		return "Banco [codigo=" + codigo + ", cnpj=" + cnpj + ", nome=" + nome + "]";
	}
	
	public Banco(int codigo, String cnpj, String nome) {
		super();
		this.codigo = codigo;
		this.cnpj = cnpj;
		this.nome = nome;
	}
	
	public Banco() {
		super();
	}
}
