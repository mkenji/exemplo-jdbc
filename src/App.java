import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class App {
	static Scanner scanner = new Scanner(System.in);
	static Banco bancoNew = new Banco();
	static BancoDao bancoDao = new BancoDao();
	
	public static void main(String[] args) throws SQLException {
		
		String sair = "";
		while(!sair.equals("Sair")) {
			System.out.println("---------------------------------");
			System.out.println("------- MENU PRINCIPAL ----------");
			
			System.out.println("Escolha a opção abaixo: ");
			System.out.println("1 - Cadastrar Banco");
			System.out.println("2 - Editar Banco");
			System.out.println("3 - Listar Banco");
			System.out.println("4 - Listar todos os bancos");
			System.out.println("5 - Deletar Banco");
			System.out.println("Sair - Sair do sistema");
			String key = scanner.next();
			
			switch (key) {
			case "1":
				cadastrarBanco();
				break;
			case "2":
				editarBanco();
				break;
			case "3":
				listarBanco();
				break;	
			case "4":
				listarTodosBancos();
				break;
			case "5":
				deletarBanco();
				break;
			case "Sair":
				break;
			}
			
		}
		
	}
	
	public static void deletarBanco() {
		System.out.println("------- DELETAR O BANCO ----------");
		System.out.println("Insira o codigo do banco para deletar: ");
		int codBancoDelete = scanner.nextInt();
		bancoDao.deletarBanco(codBancoDelete);
		System.out.println("Banco dados deletado com sucesso!");
		for(Banco itemsBanco : bancoDao.listarTodosBancos() ) {
			System.out.println(itemsBanco.toString());
		}
	}
	
	public static void listarTodosBancos() {
		System.out.println("--------- LISTAR TODOS OS BANCOS ------------");
		for(Banco itemsBanco : bancoDao.listarTodosBancos() ) {
			System.out.println(itemsBanco.toString());
		}
	}
	
	public static void listarBanco() {
		System.out.println("---------- LISTAR O BANCO -----------");
		System.out.println("Insira o codigo do banco: ");
		int codBanco = scanner.nextInt();
		System.out.println(bancoDao.listarBanco(codBanco).toString());
	}
	
	public static void cadastrarBanco() {
		System.out.println("------ CADASTRO DE BANCO -------");
		System.out.println("Insira o codigo do banco: ");
		bancoNew.setCodigo(scanner.nextInt());
		System.out.println("Insira o CNPJ do banco: ");
		bancoNew.setCnpj(scanner.next());
		System.out.println("Insira o nome do banco: ");
		bancoNew.setNome(scanner.next());
		bancoDao.inserir(bancoNew);
		System.out.println("Banco dados cadastrado com sucesso!");
	}
	
	public static void editarBanco() {
		System.out.println("------ EDITAR DE BANCO -------");
		System.out.println("Insira o codigo do banco: ");
		int codigoEdit = scanner.nextInt();
		bancoNew.setCodigo(codigoEdit);
		System.out.println("Insira o CNPJ do banco: ");
		bancoNew.setCnpj(scanner.next());
		System.out.println("Insira o nome do banco: ");
		bancoNew.setNome(scanner.next());
		bancoDao.editar(bancoNew);
		System.out.println("Banco dados editado com sucesso!");
	}
}
