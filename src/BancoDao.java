import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BancoDao {
	private Connection connect;
	private ResultSet resultSet = null;
	private PreparedStatement statement = null;

	public void inserir(Banco banco) {
		this.connect = Conexao.getConnection();
		String query = "INSERT INTO Banco (codigo, cnpj, nome) VALUES (?,?,?)";
		try {
			statement = connect.prepareStatement(query);
			statement.setInt(1, banco.getCodigo());
			statement.setString(2, banco.getCnpj());
			statement.setString(3, banco.getNome());
			statement.execute();
			close();
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Banco listarBanco(int codigo) {
		this.connect = Conexao.getConnection();
		String query = "SELECT * FROM Banco WHERE Banco.codigo = ?";
		try {
			statement = connect.prepareStatement(query);
			statement.setInt(1, codigo);
			resultSet = statement.executeQuery();
			resultSet.first();
			Banco banco = new Banco(resultSet.getInt("codigo"), resultSet.getString("cnpj"), resultSet.getString("nome"));
			close();
			return banco;
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public ArrayList<Banco> listarTodosBancos() {
		this.connect = Conexao.getConnection();
		String query = "SELECT * FROM Banco";
		try {
			statement = connect.prepareStatement(query);
			resultSet = statement.executeQuery();
			ArrayList<Banco> bancos = new ArrayList<Banco>();

			while(resultSet.next()) {
				Banco banco = new Banco(resultSet.getInt("codigo"), resultSet.getString("cnpj"), resultSet.getString("nome"));
				bancos.add(banco);
			}
			close();
			return bancos;
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean deletarBanco(int codigo) {
		this.connect = Conexao.getConnection();
		String query = "DELETE FROM Banco WHERE codigo = ?";
		try {
			statement = connect.prepareStatement(query);
			statement.setInt(1, codigo);
			boolean resultado = statement.execute();
			close();
			return resultado;
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void editar(Banco banco) {
		this.connect = Conexao.getConnection();
		String query = "UPDATE Banco SET cnpj = ?, nome = ? WHERE codigo = ?";
		try {
			statement = connect.prepareStatement(query);
			statement.setString(1, banco.getCnpj());
			statement.setString(2, banco.getNome());
			statement.setInt(3, banco.getCodigo());
			statement.execute();
			close();
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}
}
